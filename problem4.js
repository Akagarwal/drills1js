function carYears(inventory)
{
    let models=[]
    for (i = 0; i < inventory.length; i++) 
    {
        models.push(inventory[i]['car_year']);
    }
    return models;
}

module.exports = carYears;
