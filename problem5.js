const carYears = require("./problem4.js");

function modelsBeforeTwoK(inventory)
{
    let arr = carYears(inventory);
    let count = 0;
    for (let index = 0; index < arr.length; index++) 
    {
        if (arr[index] < 2000)
        {
            count++;
        }
    }
    return count;
}

module.exports= modelsBeforeTwoK;
