function retreiveData(inventory, car_ID) {

    if(!inventory || !car_ID || car_ID.length===0)
    {
        return [];
    } 
    else{
        for (i = 0; i < inventory.length; i++)
        {
            if(inventory[i]['id'] === car_ID)
            {
                return inventory[i]
            }
        }
    }
}

module.exports = retreiveData;